## Goqueryでのウェブスクレイピング for JSONアーカイブ型

## Usage
```
package models

const (
	Siteurl = ""
	Whare   = ""
	WhareJ  = ""

	// URLを前方から
	Big    = ""
	Domain = ""
	Small  = ""
	And    = ""

	// Page Limit
	Pagelim = 61
)

var (
	ArraySlice int // 取得情報のページリンクSlice length()

)

type Url struct {
	url string
}

type Data struct {
	WhereID    string // 関東など地域
	ID         int64  // id
	Name       string // 店舗名
	Slug       string // 店舗名Slug
	Url        string // オフィシャルサイト
	UrlRecruit string // 求人サイト
	AddFirst   string // 都道府県
	AddSecond  string // 県庁所在地や地域
	AddThird   string // 駅など
	Address    string // 営業所在地
	TypeBiz    string // 業種
	TypeJob    string // 職種
	Access     string // アクセス方法
	Fee        string // 給与
	Require    string // 応募資格
	Treatment  string // 待遇
	Time       string // 営業時間
	Date       string // 勤務時間
	Tel        string // 電話番号
	Line       string // LINE ID
	Email      string // Email
	Descript   string // 説明文
	Other      string // その他
}


// ページ内の取得したい情報を内包するエレメント(idやclass)を定義したりして微調整対応してください
// もちろん定義を増やすことは可能、適宜コピーやペースト、incrementで対応してください
htmlid = ""
htmlclass = ""


```

適宜、上記のようなmodelを作って、ウェブスクレイピング先各ウェブサイト用に調整してください。


## 用途

- 先駆ウェブサイトにある情報をリスト化して取得したい

## どのように出力するの？
定義したStructのJSON型で、.jsonファイルで出力します。  
ExcelやCSVに保存したい時などは、ライブラリを活用しプログラムを書く  
でもね、変換ウェブサービスが有るからJSON型から変換簡単だよ（下記参照）  
- [Convert JSON to CSV](http://www.convertcsv.com/json-to-csv.htm)

```
{
    "Access": "",
    "Address": "",
    "Cars": "",
    "Contact": "",
    "Descript": "",
    "Name": "",
    "Other": "",
    "Site": "",
    "Time": "",
    "Title": "",
    "Url": ""
},
```


## Author

[K-Terashima](https://twitter.com/6rules)
