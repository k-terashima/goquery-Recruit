package exe

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"goquery-foo.dev/models"

	"github.com/PuerkitoBio/goquery"
)

func GetHTML() {
	for i := 1; i < models.Pagelim; i++ {
		var p string
		if i == 1 {
			p = ""
		} else {
			p = fmt.Sprintf("?page=%d&", i)
		}
		page, err := goquery.NewDocument(fmt.Sprintf("https://%s.%s/%s/%s%s", models.Big, models.Domain, models.Small, models.And, p))
		if err != nil {
			log.Panic(err)
		}
		u, _ := page.Find("section.f-searchResult ").Html()
		// page.Find("section.f-searchResult ").Each(func(_ int, s *goquery.Selection) {
		// 	url, _ := s.Attr("href")
		// 	urls = append(urls, url)
		//
		// })
		// u, err := json.Marshal(urls)
		// if err != nil {
		// 	log.Panic(err)
		// }
		_, err = os.Create(fmt.Sprintf("./db/link_%d.json", i))
		ioutil.WriteFile(fmt.Sprintf("./db/link_%d.json", i), []byte(u), 644)
	}
}
