package exe

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
)

func GetHTMLforLink() {
	// HTMLからHTML Arrayにクレンジング
	jData, err := ioutil.ReadFile("./db/link.json")
	if err != nil {
		log.Panic(err)
	}

	// []byte型をstring型にして配列を作る
	j := string(jData)
	j = strings.Replace(j, "[", "", -1)
	j = strings.Replace(j, "]", "", -1)
	j = strings.Replace(j, ",", " ", -1)
	j = strings.Replace(j, "\"", "", -1)
	a := strings.Fields(j)

	for i, v := range a {
		page, err := goquery.NewDocument(v)
		if err != nil {
			log.Panic(err)
		}
		u, _ := page.Find("main").Html()

		fmt.Println(i, v)

		_, err = os.Create(fmt.Sprintf("./shops/%d.json", i))
		ioutil.WriteFile(fmt.Sprintf("./shops/%d.json", i), []byte(u), 644)
		time.Sleep(time.Second * 1)
	}
}
